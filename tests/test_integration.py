import time
import requests
from urllib.parse import urljoin
from urllib3.util.retry import Retry
from requests.adapters import HTTPAdapter
import selenium
import pytest

from selenium import webdriver

pytest_plugins = ["docker_compose"]

@pytest.fixture(scope="function")
def wait_for_nginx_and_selenium(session_scoped_container_getter):
    """Wait for the api from my_api_service to become responsive"""
    request_session = requests.Session()
    retries = Retry(total=5,
                    backoff_factor=0.1,
                    status_forcelist=[500, 502, 503, 504])
    request_session.mount('http://', HTTPAdapter(max_retries=retries))

    nginx = session_scoped_container_getter.get("nginx").network_info[0]
    nginx_url = f"http://{nginx.hostname}:{nginx.host_port}"
    assert request_session.get(nginx_url)
    selenium = session_scoped_container_getter.get("selenium").network_info[0]
    selenium_url = f"http://{selenium.hostname}:{selenium.host_port}/wd/hub"
    assert request_session.get(selenium_url)
    return nginx_url, selenium_url


def test_bokeh_flask_nginx(wait_for_nginx_and_selenium):
    nginx_url, selenium_url = wait_for_nginx_and_selenium
    desired_capabilities = webdriver.common.desired_capabilities.DesiredCapabilities.CHROME.copy()
    driver = webdriver.Remote(command_executor="http://localhost:4444/wd/hub", desired_capabilities=desired_capabilities)
    driver.get("http://nginx:8081")
    elts = driver.find_elements_by_tag_name('script')
    assert len([x for x in elts if 'bokeh' in x.get_attribute("innerHTML")]) > 0
    driver.close()
    driver.quit()

