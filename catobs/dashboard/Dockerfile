FROM python:3.8 as base

FROM base as builder

COPY ./requirements.txt /requirements.txt
RUN pip wheel -r /requirements.txt --no-cache-dir --no-deps --wheel-dir /wheels

FROM python:3.8-slim
ENV USER=app
ENV HOME=/home/$USER
RUN mkdir -p $HOME \
    && addgroup --system "$USER" \
    && adduser \
        --system \
        --no-create-home \
        --disabled-password \
        --gecos "" \
        --ingroup "$USER" \
        "$USER"
ENV APP_HOME=$HOME/src
RUN mkdir $APP_HOME
WORKDIR $APP_HOME
RUN chown -R app:app $APP_HOME

ENV PYTHONUNBUFFERED 1
ENV BOKEH_RESOURCES="inline"
EXPOSE 5006


COPY --from=builder /wheels /wheels
RUN pip install --no-cache /wheels/*
RUN rm -r /wheels
COPY . $APP_HOME

USER app