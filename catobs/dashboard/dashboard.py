# holoviews.py

import holoviews as hv
import panel as pn
import numpy as np
import pymongo
import streamz
import asyncio
import concurrent.futures
import pandas as pd
from streamz import Stream, from_iterable
from holoviews.streams import Buffer
from streamz.dataframe import DataFrame
from motor.motor_tornado import MotorClient
from holoviews.operation.timeseries import rolling
import sys
from tornado import gen
from tornado.ioloop import IOLoop
import time
import param
from asyncio import Queue
import logging
import os

LITERS_PER_FRAME = 2.32
TAXA='Copepod,fecal pellet long,fecal pellet blob,amphipod,marine_snow,Appendicularian,diatom_chain,tentacles,Chaetognath,Siphonophore,Euphausid,center_spot_line,Ctenophore,radiolarian,Siphonophore Sphaeronectes,Siphonophore diphyid,Acantharian skinny star straight,medusa skinny star squiggly,medusa,Salpa aspera,polychaete,Decapod,Foraminifera_Calcarina,Polychaete_type2,fish,radiolarian colony,pterpod'.split(',')

hv.extension('bokeh')
pn.extension()

LOG_FORMAT = "%(levelname)s %(asctime)s - %(message)s"
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(logging.Formatter(LOG_FORMAT))
logger = logging.getLogger(__name__)
logger.addHandler(stream_handler)
logger.setLevel(logging.DEBUG)

client = MotorClient('mongo1', replicaset='rs0')
db = client.__getattr__("yolo")
collection = db.detections

last_imgid = 0

stream = Stream(asynchronous=False)

post_queue = Queue()

img_numbers = set()
max_img_id = 2



def post_to_dataframe(post):
    df = pd.DataFrame.from_dict({k: [v] for k, v in post.items() if k != '_id'}).value_counts().reset_index(name='count')
    df['concentration'] = df['count'] / LITERS_PER_FRAME
    df['time'] = df['time'].astype('datetime64[ns]')
    return df

def process_dataframe(ldf):
    stream.emit(ldf.sort_values(by='time'))

async def watch_collection(interval=0.5):

    global last_imgid


    async with await client.start_session() as s:
        dfs = []
        cursor = collection.find(session=s)
        for x in await cursor.to_list(length=10000):
            dfs.append(post_to_dataframe(x))
        if len(dfs) > 0:
            df = pd.concat(dfs)
            last_imgid = max(df['imgid'].astype(int))
            process_dataframe(df)
        #
        # def callback(result, error):
        #     global last_imgid
        #     print(result, error)
        #     if error:
        #         raise error
        #     elif result:
        #         print(result)
        #         last_imgid = max(last_imgid, result['imgid'])
        #         stream.emit(post_to_dataframe(result))
        # counter = 0

        async for update in collection.watch(session=s):
            await post_queue.put(update['fullDocument'])

        # while True:
        #     collection.find({'imgid': {'$gt': last_imgid}}, session=s).each(callback=callback)
        #     print(f'{counter}: {last_imgid} ({await collection.count_documents({})} total)')
        #     counter += 1
        #     await asyncio.sleep(interval)

class TaxonCounter(param.Parameterized):
    confidence = param.Number(default=0.5, bounds=(0, 1), step=0.01)

    @param.depends('confidence')
    def plot(self, data):
        tab = hv.Table(data[data.confidence >= self.confidence]).sort(['time'])
        return hv.Bars(tab, kdims=['taxon'],vdims=['count']).aggregate(function=np.sum).sort()

class TaxonTimeConcentration(param.Parameterized):
    confidence = param.Number(default=0.5, bounds=(0, 1), step=0.01)
    window = param.Number(default=10, bounds=(1, 100), step=1)
    taxon = param.ObjectSelector(default='Copepod', objects=TAXA)

    @param.depends('confidence', 'taxon')
    def plot(self, data):
        conf_idx = data.confidence >= self.confidence
        data = data[conf_idx].reset_index()
        taxon_idx = (data['taxon'] == self.taxon)
        data.loc[~taxon_idx, 'concentration'] = 0
        data['imgid'] = data['imgid'].astype(float)
        tab = hv.Table(data, ['time', 'imgid'], ['concentration'])
        curve = tab.to.scatter(['imgid'], ['concentration'], []).opts(framewise=True)
        return curve

class TaxonTimeConcentrationStackPlot(param.Parameterized):
    confidence = param.Number(default=0.5, bounds=(0, 1), step=0.01)
    window = param.Number(default=10, bounds=(1, 100), step=1)

    @param.depends('confidence', 'taxon')
    def plot(self, data):
        conf_idx = data.confidence >= self.confidence
        data = data[conf_idx].reset_index()

        tab = hv.Table(data)
        curve = hv.Curve(tab, kdims=['time'], vdims=['concentration']).opts(framewise=True)
        return curve


try:
    df = pd.DataFrame({x: [] for x in ['imgid', 'time', 'taxon', 'confidence', 'x', 'y', 'w', 'h', 'count', 'concentration']})
    df = df.astype({
        'imgid': int,
        'time': 'datetime64[ns]',
        'taxon': str,
        'confidence': float,
        'x': float,
        'y': float,
        'w': float,
        'h': float,
        'count': int,
        'concentration': float
    })
    sdf = DataFrame(stream, example=df)



    buffer = Buffer(sdf, length=10000000)
    counter = TaxonCounter()
    counter_dmap = hv.DynamicMap(counter.plot, streams=[buffer]).opts(width=500, xrotation=60, show_grid=True)
    counter_row = pn.Row(pn.panel(counter.param, parameters=['confidence']), counter_dmap)
    tf = TaxonTimeConcentration()
    tf_dmap = hv.DynamicMap(tf.plot, streams=[buffer]).opts(width=500, xrotation=60, show_grid=True)
    tf_row = pn.Row(tf.param, rolling(tf_dmap, rolling_window=tf.param.window))

    imgid_slider = pn.widgets.IntSlider(start=1, end=max_img_id, step=1)
    img_pane = pn.pane.JPG('https://upload.wikimedia.org/wikipedia/en/9/95/Test_image.jpg', width=500)
    def update_img_pane(imgid):
        if len(img_numbers) and not (imgid.obj.value in img_numbers):
            imgid.obj.value = min(img_numbers, key=lambda x: abs(x-imgid.obj.value))
        img_str = f'/home/app/images/Basler_avA2300-25gm__22955661__20200315_075501348_{imgid.obj.value}.jpg'
        img_pane.object = img_str
    imgid_slider.param.watch(update_img_pane, 'value')
    img_row = pn.Row(pn.WidgetBox(imgid_slider), img_pane)

    asyncio.get_running_loop().create_task(watch_collection())


    async def process_queue():
        global max_img_id
        posts = []
        loop = asyncio.get_running_loop()
        while loop.is_running():
            while not post_queue.empty():
                posts.append(post_to_dataframe(await post_queue.get()))
            if len(posts) > 0:
                df = pd.concat(posts)
                new_img_numbers = df['imgid'].astype(int).tolist()
                img_numbers.update(new_img_numbers)
                if (new_max_img_id := max(new_img_numbers)) > max_img_id:
                    max_img_id = new_max_img_id
                    imgid_slider.end = max_img_id
                process_dataframe(df)
            await asyncio.sleep(1)

    asyncio.get_running_loop().create_task(process_queue())
    pn.Column(counter_row, tf_row, img_row).servable(title="Test")
finally:
    pass
# finally:
#     if change_stream is not None and change_stream.alive:
#         cb.stop()
#         change_stream.close()
#     print("Exiting!")
