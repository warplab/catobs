import os
import cv2
import glob
from pathlib import Path
from utils.datasets import letterbox
import numpy as np
import concurrent
from multiprocessing import Queue
import re

img_formats = ['bmp', 'jpg', 'jpeg', 'png', 'tif', 'tiff', 'dng', 'webp']  # acceptable image suffixes

# https://stackoverflow.com/questions/4836710/is-there-a-built-in-function-for-string-natural-sort
def natural_sort_key(s, _nsre=re.compile('([0-9]+)')):
    return [int(text) if text.isdigit() else text.lower()
            for text in _nsre.split(s)]

class LoadImagesConcurrent:  # for inference
    def __init__(self, path, img_size=640, stride=32, queue_size=20, threads=4):
        p = str(Path(path).absolute())  # os-agnostic absolute path
        if '*' in p:
            files = sorted(glob.glob(p, recursive=True), key=natural_sort_key)  # glob
        elif os.path.isdir(p):
            files = sorted(glob.glob(os.path.join(p, '*.*')), key=natural_sort_key)  # dir
        elif os.path.isfile(p):
            files = [p]  # files
        else:
            raise Exception(f'ERROR: {p} does not exist')

        images = [x for x in files if x.split('.')[-1].lower() in img_formats]
        ni = len(images)

        self.img_size = img_size
        self.stride = stride
        self.files = images
        self.nf = ni # number of files
        self.mode = 'image'
        self.cap = None
        assert self.nf > 0, f'No images or videos found in {p}. ' \
                            f'Supported formats are:\nimages: {img_formats}'
        self.q = Queue(maxsize=queue_size)
        self.executor = concurrent.futures.ThreadPoolExecutor(4)
        for im in self.files:
            self.executor.submit(lambda i: self.q.put((i, cv2.imread(i))), im)


    def __iter__(self):
        self.count = 0
        return self

    def __next__(self):
        if self.count == self.nf:
            raise StopIteration
        # path = self.files[self.count]
        #
        # # Read image
        # self.count += 1
        # img0 = cv2.imread(path)  # BGR
        path, img0 = self.q.get()
        self.count += 1
        assert img0 is not None, 'Image Not Found ' + path
        print(f'image {self.count}/{self.nf} {path}: ', end='')

        # Padded resize
        img = letterbox(img0, self.img_size, stride=self.stride)[0]

        # Convert
        img = img[:, :, ::-1].transpose(2, 0, 1)  # BGR to RGB, to 3x416x416
        img = np.ascontiguousarray(img)

        return path, img, img0, self.cap

    def __len__(self):
        return self.nf  # number of files
