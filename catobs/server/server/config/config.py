# project/server/config.py

import os

class BaseConfig(object):
    """Base configuration."""
    APP_NAME = os.getenv("APP_NAME", "CatObs")


class DevelopmentConfig(BaseConfig):
    """Development configuration."""
    pass


class TestingConfig(BaseConfig):
    """Testing configuration."""

    PRESERVE_CONTEXT_ON_EXCEPTION = False
    TESTING = True


class ProductionConfig(BaseConfig):
    """Production configuration."""
    pass
