from flask import render_template, Blueprint
from bokeh.client import pull_session
from bokeh.embed import server_session
from flask import send_from_directory
import panel

main_blueprint = Blueprint("main", __name__)


@main_blueprint.route("/")
def home():
    with pull_session(url="http://dashboard:5006/dashboard") as session:
        # generate a script to load the customized session
        script = server_session(session_id=session.id, url='http://localhost:8081/dashboard')
        # use the script in the rendered page
    return render_template("main/home.html", script=script, template="Flask")