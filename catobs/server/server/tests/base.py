# project/server/tests/base.py


from flask_testing import TestCase

from ..app import create_app
from werkzeug.utils import import_string
app = create_app()


class BaseTestCase(TestCase):
    def create_app(self):
        cfg = import_string("server.config.TestingConfig")
        app.config.from_object(cfg)
        return app

    def setUp(self):
        pass

    def tearDown(self):
        pass
