
import unittest
import os

from flask import current_app
from flask_testing import TestCase
from werkzeug.utils import import_string
from ..app import create_app

app = create_app()


class TestDevelopmentConfig(TestCase):
    def create_app(self):
        cfg = import_string("server.config.DevelopmentConfig")
        app.config.from_object(cfg)
        return app

    def test_app_is_development(self):
        self.assertFalse(current_app.config["TESTING"])
        self.assertFalse(current_app is None)


class TestTestingConfig(TestCase):
    def create_app(self):
        cfg = import_string("server.config.TestingConfig")
        app.config.from_object(cfg)
        return app

    def test_app_is_testing(self):
        self.assertTrue(current_app.config["TESTING"])


class TestProductionConfig(TestCase):
    def create_app(self):
        cfg = import_string("server.config.ProductionConfig")
        app.config.from_object(cfg)
        return app

    def test_app_is_production(self):
        self.assertFalse(current_app.config["TESTING"])


if __name__ == "__main__":
    unittest.main()
