import os

from flask import Flask, render_template, send_from_directory
from werkzeug.utils import import_string


def create_app(script_info=None):

    # instantiate the app
    app = Flask(
        'flaskServer',
        template_folder="server/client/templates",
        static_folder="server/client/static",
    )

    # set config
    app_settings = os.getenv(
        "APP_SETTINGS", "server.config.ProductionConfig"
    )
    cfg = import_string(app_settings)
    app.config.from_object(cfg)

    from ..main.views import main_blueprint

    app.register_blueprint(main_blueprint)

    # error handlers
    @app.errorhandler(401)
    def unauthorized_page(error):
        return render_template("errors/401.html"), 401

    @app.errorhandler(403)
    def forbidden_page(error):
        return render_template("errors/403.html"), 403

    @app.errorhandler(404)
    def page_not_found(error):
        return render_template("errors/404.html"), 404

    @app.errorhandler(500)
    def server_error_page(error):
        return render_template("errors/500.html"), 500

    @app.route("/static/<path:filename>")
    def staticfiles(filename):
        return send_from_directory(app.static_folder, filename)

    # shell context for flask cli
    @app.shell_context_processor
    def ctx():
        return {"app": app}

    return app
