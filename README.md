CatObs: a visual exploration tool for streaming categorical data.

Install docker:

~~~shell
sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io
~~~



Install docker-compose:
~~~shell
sudo curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
~~~

Install Nvidia drivers:

~~~shell
sudo apt-get install -y cuda-drivers
~~~

Install nvidia-docker2:
~~~shell
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -

distribution=$(. /etc/os-release;echo $ID$VERSION_ID)

curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

sudo apt-get update
sudo apt-get install -y nvidia-docker2

sudo pkill -SIGHUP dockerd
~~~

To run (make sure to set the `YOLO_SOURCE` env variable in `.env` to a folder with plankton pictures):

~~~shell
sudo docker-compose -f docker-compose.yml up
~~~

The dashboard is located at <http://localhost:8081>.

To test (in a `python` enviornment with the `tox` package):

~~~shell
tox
~~~

